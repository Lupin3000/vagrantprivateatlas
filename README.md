Usage Info
==========

Example Usage (use image)
-------------------------

```shell
# create new VM
$ docker-machine create -d virtualbox Atlas

# list VM`s (optional)
$ docker-machine ls

# pointing shell
$ eval $(docker-machine env Atlas)

# pull from DockerHub
$ docker pull slorenz/atlas

# create new container
$ docker run -d --name atlas -p 80:80 -v <path>:/usr/share/nginx/html/atlas slorenz/atlas

# list container (optional)
$ docker ps -a
```

Example Usage (build own image)
-------------------------------

```shell
# create new VM
$ docker-machine create -d virtualbox Atlas

# list VM`s (optional)
$ docker-machine ls

# pointing shell
$ eval $(docker-machine env Atlas)

# build from Dockerfile
$ docker build -t slorenz/atlas .

# list images (optional)
$ docker images

# create new container
$ docker run -d --name atlas -p 80:80 -v <path>:/usr/share/nginx/html/atlas slorenz/atlas

# list container (optional)
$ docker ps -a
```

Example Usage (docker-compose)
-------------------------------

```shell
# create new VM
$ docker-machine create -d virtualbox Atlas

# list VM`s (optional)
$ docker-machine ls

# pointing shell
$ eval $(docker-machine env Atlas)

# build and run via docker-compose
$ docker-compose up -d

# list composed (optional)
$ docker-compose ps
```

Create new or update boxes
--------------------------

1. on mounted volume ("/usr/share/nginx/html/atlas/boxes") add your new BaseBox
2. on mounted volume ("/usr/share/nginx/html/atlas") add your new corresponding JSON
3. refresh start page on browser
