FROM centos:7

MAINTAINER steffen lorenz <slorenz@noreplay.com>

RUN yum update -y && yum install -y epel-release
RUN yum install -y nginx php php-fpm python-setuptools
RUN yum clean all
RUN easy_install supervisor

RUN /usr/bin/sed -i '/;cgi.fix_pathinfo=1/c\cgi.fix_pathinfo=0' /etc/php.ini
RUN /usr/bin/sed -i '/listen = 127.0.0.1:9000/c\listen = /var/run/php-fpm/php-fpm.sock' /etc/php-fpm.d/www.conf
RUN /usr/bin/sed -i '/;listen.owner = nobody/c\listen.owner = nobody' /etc/php-fpm.d/www.conf
RUN /usr/bin/sed -i '/;listen.group = nobody/c\listen.group = nobody' /etc/php-fpm.d/www.conf
RUN /usr/bin/sed -i '/user = apache/c\user = nginx' /etc/php-fpm.d/www.conf
RUN /usr/bin/sed -i '/group = apache/c\group = nginx' /etc/php-fpm.d/www.conf
RUN /usr/bin/mkdir -p /usr/share/nginx/html/atlas/boxes

COPY files/nginx.conf /etc/nginx/nginx.conf
COPY files/supervisord.conf /usr/etc/supervisord.conf
COPY files/index.php /usr/share/nginx/html/index.php
COPY files/scripts.js /usr/share/nginx/html/scripts.js
COPY files/styles.css /usr/share/nginx/html/styles.css

VOLUME ["/usr/share/nginx/html/atlas"]

EXPOSE 80

ENTRYPOINT ["/usr/bin/supervisord"]