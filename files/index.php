<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<title>Private Atlas - Repository</title>
	<link rel="stylesheet" href="styles.css">
</head>
<body>
	<div id="wrapper">
		<h1>Private Atlas - Repository</h1>
		<div id="boxview">
			<h2>BaseBox Overview</h2>
<?php
function human_filesize($bytes, $decimals = 2) {
	$size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
	$factor = floor((strlen($bytes) - 1) / 3);
	return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}

$directory = 'atlas/boxes/';
$files = array_slice(scandir($directory), 2);

if (empty($files)) {
	print '<div class="RepositoryListItem">';
	print '<a href="./' . $directory . '">No BaseBox found.</a>';
	print '</div>';
}

foreach($files as $val) {
	$file = $directory . $val;
	$size = '<span> - ' . human_filesize(filesize($file)) . '</span>';

	print '<div class="RepositoryListItem">';
	print '<a href="./' . $directory . $val . '" title="' . $val . '">' . $val . $size . '</a>';
	print '</div>';
}
?>
		</div>
		<div id="footer">
			<a href="#" title="Show Help" onclick="helpFunction('unhide');">Show help</a>
		</div>
	</div>
	<div id="help">
		<div id="innerHelp">
			<hr>
			<h3>Usage of Boxes from private Atlas</h3>
			<code>
				# create new project directory<br>
				$ mkdir ~/tutorial &amp;&amp; cd ~/tutorial<br>
				<br>
				# add base box from repository<br>
				$ vagrant box add demo/centos7 http://&lt;ip|domain&gt;/atlas/centos.json<br>
				<br>
				# list all boxes<br>
				$ vagrant box list<br>
				...<br>
				demo/centos7  (virtualbox, 0.1.0)<br>
				<br>
				# create new Vagrant project<br>
				$ vagrant init demo/centos7<br>
				<br>
				# edit generated Vagrantfile<br>
				$ vim Vagrantfile<br>
				<br>
				# start Vagrant<br>
				$ vagrant up
			</code>
			<a href="#" title="Close Help" onclick="helpFunction('hide');">Close help</a>
			<hr>
			<h3>Create new Box on private Atlas</h3>
			<p>
				- Upload new BaseBox into "/usr/share/nginx/html/atlas/boxes"<br>
				- Create or modify the corresponding JSON file into "/usr/share/nginx/html/atlas"<br>
			</p>
			<code>
				{<br>
				"name": "demo/centos7",<br>
				"description": "This box contains CentOS 7 64-bit.",<br>
				"versions": [{<br>
				&nbsp; &nbsp;"version": "0.1.0",<br>
				&nbsp; &nbsp;"providers": [{<br>
				&nbsp; &nbsp; &nbsp; &nbsp;"name": "virtualbox",<br>
				&nbsp; &nbsp; &nbsp; &nbsp;"url": "http://&lt;ip|domain&gt;/atlas/boxes/centos.json",<br>
				&nbsp; &nbsp; &nbsp; &nbsp;"checksum_type": "sha1",<br>
				&nbsp; &nbsp; &nbsp; &nbsp;"checksum": "99e6d7fc44cccabdfc6ed9ce178ca65fd9dcbac8"<br>
				&nbsp; &nbsp;}]<br>
				}]<br>
				}
			</code>
			<a href="#" title="Close Help" onclick="helpFunction('hide');">Close help</a>
			<br>
			<hr>
			<br>
		</div>
	</div>
	<script src="scripts.js"></script>
</body>
</html>